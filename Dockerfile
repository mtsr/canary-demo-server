FROM rust AS build

# create empty shell project
RUN USER=root cargo new --bin canary-demo-server

WORKDIR /canary-demo-server
COPY Cargo.lock Cargo.toml ./

RUN cargo build --release 
RUN find . -iname 'canary[-_]demo[-_]server*' | xargs rm -rf && rm -rf src

COPY src/ src/

RUN cargo build --release

# ------------------------------

FROM debian:8-slim

WORKDIR /usr/canary-demo-server/
COPY --from=build /canary-demo-server/target/release/canary-demo-server .

ENTRYPOINT ["/usr/canary-demo-server/canary-demo-server"]
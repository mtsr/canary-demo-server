#![deny(warnings)]
extern crate clap;
extern crate env_logger;
extern crate futures;
extern crate hyper;
extern crate rand;
extern crate tokio_signal;
extern crate tokio_timer;
#[macro_use]
extern crate log;

use clap::{App, Arg};
use futures::{future, Stream};
use hyper::rt::{self, Future};
use hyper::service::service_fn;
use hyper::{Body, Response, Server};
use rand::distributions::{Distribution, LogNormal};
use rand::prelude::*;
use std::net::SocketAddr;
use std::time::Duration;
use tokio_signal::unix::{Signal, SIGINT, SIGTERM};

static PHRASE: &'static [u8] = b"Hello World!";
static MILLIS_PER_SEC: f64 = 1.0e3;
static NANOS_PER_SEC: f64 = 1.0e9;

fn main() {
    let env = env_logger::Env::default().filter_or(env_logger::DEFAULT_FILTER_ENV, "info");
    env_logger::Builder::from_env(env).init();

    let matches = App::new("canary-demo-client")
        .version("0.1")
        .about("...")
        .author("Jonas Matser")
        .arg(
            Arg::with_name("listen-address")
                .short("a")
                .long("listen-address")
                .help("Sets the address the server listens on")
                .default_value("0.0.0.0:3000")
                .validator(|val| {
                    val.parse::<SocketAddr>()
                        .map(|_| ())
                        .map_err(|_| "Not a valid listen address".to_owned())
                }),
        )
        .arg(
            Arg::with_name("error-rate")
                .short("e")
                .long("error-rate")
                .help("Sets fraction of 500 errors")
                .default_value("0.0")
                .validator(|val| {
                    val.parse::<f64>()
                        .map(|_| ())
                        .map_err(|_| "Not a valid fraction".to_owned())
                }),
        )
        .arg(
            Arg::with_name("latency-mean")
                .short("m")
                .long("latency-mean")
                .help("Sets latency mean in milliseconds")
                .default_value("0.0")
                .validator(|val| {
                    val.parse::<f64>()
                        .map(|_| ())
                        .map_err(|_| "Not a valid fraction".to_owned())
                }),
        )
        .arg(
            Arg::with_name("latency-std-dev")
                .short("s")
                .long("latency-std-dev")
                .help("Sets latency standard deviation in milliseconds")
                .default_value("0.0")
                .validator(|val| {
                    val.parse::<f64>()
                        .map(|_| ())
                        .map_err(|_| "Not a valid fraction".to_owned())
                }),
        )
        .get_matches();

    let addr: SocketAddr = matches
        .value_of("listen-address")
        .unwrap()
        .parse()
        .expect("Not a valid listen address");

    // panicfree unwraps because of validator
    let error_rate: f64 = matches.value_of("error-rate").unwrap().parse().unwrap();
    let latency_mean_millis: f64 = matches
        .value_of("latency-mean")
        .unwrap()
        .parse::<f64>()
        .unwrap();
    let latency_std_dev_millis: f64 = matches
        .value_of("latency-std-dev")
        .unwrap()
        .parse::<f64>()
        .unwrap();

    let latency_distribution_millis = if latency_mean_millis != 0.0 || latency_std_dev_millis != 0.0
    {
        Some(LogNormal::new(
            // convert to logarithmized mean (see https://en.wikipedia.org/wiki/Log-normal_distribution#Notation)
            (latency_mean_millis
                / (1.0 + latency_std_dev_millis.powi(2) / latency_mean_millis.powi(2)).sqrt())
            .ln(),
            // convert to logarithmized variance
            (1.0 + latency_std_dev_millis.powi(2) / latency_mean_millis.powi(2))
                .ln()
                .sqrt(),
        ))
    } else {
        None
    };

    info!(
        "Listening on {} with latency mean {:?} and std-dev {:?}",
        addr, latency_mean_millis, latency_std_dev_millis
    );

    // new_service is run for each connection, creating a 'service'
    // to handle requests for that specific connection.
    let new_service = move || {
        let error_rate = error_rate;

        service_fn(move |req| {
            let latency = if let Some(latency_distribution) = latency_distribution_millis {
                let latency = latency_distribution.sample(&mut thread_rng());
                let latency_secs = (latency / MILLIS_PER_SEC).trunc() as u64;
                let latency_millis = ((latency / MILLIS_PER_SEC).fract() * NANOS_PER_SEC) as u32;

                Duration::new(latency_secs, latency_millis)
            } else {
                Duration::new(0, 0)
            };

            tokio_timer::sleep(latency).and_then(move |_| match thread_rng().gen::<f64>() {
                x if x < error_rate => {
                    info!("500 ({:?}): {:?}", latency, req);
                    future::ok(
                        Response::builder()
                            .status(500)
                            .body(Body::from("Random error"))
                            .unwrap(),
                    )
                }
                _ => {
                    info!("200 ({:?}): {:?}", latency, req);
                    future::ok(Response::new(Body::from(PHRASE)))
                }
            })
        })
    };

    let (tx, rx) = futures::sync::oneshot::channel::<()>();

    let server = Server::bind(&addr)
        .serve(new_service)
        .with_graceful_shutdown(rx)
        .map_err(|e| eprintln!("server error: {}", e));

    println!("Listening on http://{}", addr);

    // Create a stream for each of the signals we'd like to handle.
    let sigint = Signal::new(SIGINT).flatten_stream();
    let sigterm = Signal::new(SIGTERM).flatten_stream();

    // Use the `select` combinator to merge these two streams into one
    let stream = sigint.select(sigterm);

    let shutdown = stream
        .into_future()
        .and_then(move |_| {
            println!("Shutting down gracefully...");
            let _ = tx.send(());
            Ok(())
        })
        .map_err(|_e| eprintln!("server error"));

    rt::run(future::ok(()).join(server).join(shutdown).map(|_| ()));
}
